<?php


//========================================================= CONNEXION A LA BASE DE DONNEES =============================================================


$servername = "localhost";
$dbname = "ppe";
$username = "root"; 				//à modifier
$password = "000";


try {
	$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
	// Attribution du tag (étiquette) 'exception' aux erreurs de connexion
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	// echo "Connexion à la base de données réussie ! <br>"; 
}
	// Capture de l'erreur de connexion entant qu'exception dans la variable $e et affichage du message d'erreur
catch(PDOException $e){
	echo "Echec de la connexion : " . $e->getMessage();
}

?>