package PPE3_classes.PPE3_modele;

import javax.rmi.CORBA.Util;
import javax.xml.transform.Result;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;

public class Ticket {
    Ticket t = null;
    int numTicket;
    int numProduit;
    String statut = "O";
    int numPro = Utilisateur.u.getNumPro();
    Date dateTicket = new Date();
    Date clotTicket = null;
    String description;
    int complex;
    int numTechnicien;

   public Ticket(int numTicket, int numProduit, String statut, int numPro, Date dateTicket, Date clotTicket, String description, int complex, int numTechnicien) {
        this.numTicket = numTicket;
        this.numProduit = numProduit;
        this.statut = statut;
        this.dateTicket = dateTicket;
        this.clotTicket = clotTicket;
        this.numPro = numPro;
        this.description = description;
        this.complex = complex;
        this.numTechnicien = numTechnicien;
    }


    public static boolean ouvrirTicket(Ticket ticket) {
        boolean otto;
        String strSQL = "INSERT INTO tickets(numProduit, statut, numPro, description) VALUES (?, ?, ?, ?)";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection = DriverManager.getConnection(UtilisateurDB.URL, Utilisateur.u.identifiant, Utilisateur.u.motDePasse);

            PreparedStatement pstmt = connection.prepareStatement(strSQL);
            pstmt.setInt(1, ticket.numProduit);
            pstmt.setString(2, ticket.statut);
            pstmt.setInt(3, ticket.numPro);
            pstmt.setString(4, ticket.description);


            pstmt.execute();
            otto = true;
        }
        catch (ClassNotFoundException e)  {
            e.printStackTrace();
            otto = false;
        }
        catch (SQLException e)  {
            e.printStackTrace();
            otto = false;
        }
        return otto;
    }

    public Ticket consulterTicket(int numTicket) {
        Ticket t = null;

        String strSQL = "SELECT * FROM tickets WHERE numTicket = ?";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection = DriverManager.getConnection(UtilisateurDB.URL, Utilisateur.u.identifiant, Utilisateur.u.motDePasse);

            PreparedStatement pstmt = connection.prepareStatement(strSQL);
            pstmt.setInt(1, numTicket);

            ResultSet rset = pstmt.executeQuery();

            if (rset.next() ){
                // récupération des détails du ticket
                int numProduit = rset.getInt("numProduit");
                int numPro = rset.getInt("numPro");
                String statut = rset.getString("statut");
                Date dateTicket = rset.getDate("dateTicket");
                Date clotTicket = rset.getDate("clotTicket");
                String description  = rset.getString("description");
                int complex = rset.getInt("complex");
                int numTechnicien = rset.getInt("numTechnicien");

                t = new Ticket(numTicket, numProduit, statut, numPro, dateTicket, clotTicket, description, complex, numTechnicien);
            }
        }
        catch (ClassNotFoundException e)  {
            e.printStackTrace();
        }
        catch (SQLException e)  {
            e.printStackTrace();
        }
        return t;
    }

   public static ArrayList afficherTickets() {
       //si l'utilisateur n'est pas un technicien,
       //seuls les tickets de cet utilisateur seront affichés
       String utilisateur;
       if (Utilisateur.u.typePro == 'T') {
           utilisateur = "";
       }
       else {
           utilisateur = "WHERE numPro = ?";
       }

       ArrayList<Ticket> lesTickets = new ArrayList();

       try {
           Class.forName("com.mysql.cj.jdbc.Driver");
           Connection connection = DriverManager.getConnection(UtilisateurDB.URL, Utilisateur.u.identifiant, Utilisateur.u.motDePasse);

               String strSQL = "SELECT * FROM tickets" + utilisateur;
               PreparedStatement pstmt = connection.prepareStatement(strSQL);

                pstmt.setInt(1, Utilisateur.u.numPro);
               //pstmt2.setInt(1, numTicket);

               //ArrayList<Object> lesValeurs = new ArrayList();
               ResultSet rset = pstmt.executeQuery();

               while (rset.next()) {
                   // récupération des détails du ticket
                   int leNumTicket = rset.getInt("numTicket");
                   int numProduit = rset.getInt("numProduit");
                   int numPro = rset.getInt("numPro");
                   String statut = rset.getString("statut");
                   Date dateTicket = rset.getDate("dateTicket");
                   Date clotTicket = rset.getDate("clotTicket");
                   String description = rset.getString("description");
                   int complex = rset.getInt("complex");
                   int numTechnicien = rset.getInt("numTechnicien");

                   Ticket lesValeurs = new Ticket(leNumTicket, numProduit, statut, numPro, dateTicket, clotTicket, description, complex, numTechnicien);

                   /*lesValeurs.add(numTicket);
                   lesValeurs.add(numProduit);
                   lesValeurs.add(numPro);
                   lesValeurs.add(statut);
                   lesValeurs.add(dateTicket);
                   lesValeurs.add(clotTicket);
                   lesValeurs.add(description);
                   lesValeurs.add(complex);
                   lesValeurs.add(numTechnicien);*/

                   lesTickets.add(lesValeurs);
               }
           //}
       }
       catch (ClassNotFoundException e)  {
               e.printStackTrace();
       }
       catch (SQLException e)  {
               e.printStackTrace();
       }
       return  lesTickets;
   }



    public String repondreTicket(int numTicket) {
        //réservé aux techniciens
        String s = null;
        if (Utilisateur.u.typePro == 'T') {
            String strSQL = "SELECT email FROM tickets WHERE numTicket = ?";

            try {
                Class.forName("com.mysql.cj.jdbc.Driver");
                Connection connection = DriverManager.getConnection(UtilisateurDB.URL, Utilisateur.u.identifiant, Utilisateur.u.motDePasse);

                PreparedStatement pstmt = connection.prepareStatement(strSQL);
                pstmt.setInt(1, numTicket);

                ResultSet rset = pstmt.executeQuery();

                if (rset.next()) {
                    // récupération des détails du ticket
                    s = rset.getString("email");
                }

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                s = e.getMessage();

            } catch (SQLException e) {
                s = e.getMessage();
            }
        } else {
            s = "action incorrecte";
        }
        return s;
    }

    public boolean fermerTicket(int numTicket) {
        boolean otto;
        String strSQL = "UPDATE tickets SET statut = 'F' WHERE numTicket = ?";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection = DriverManager.getConnection(UtilisateurDB.URL, Utilisateur.u.identifiant, Utilisateur.u.motDePasse);

            PreparedStatement pstmt = connection.prepareStatement(strSQL);
            pstmt.setInt(1, numTicket);

            ResultSet rset = pstmt.executeQuery();

            System.out.println(rset);
            otto = true;
        }
        catch (ClassNotFoundException e)  {
            e.printStackTrace();
            otto = false;
        }
        catch (SQLException e)  {
            e.printStackTrace();
            otto = false;
        }
        return otto;
    }
}
