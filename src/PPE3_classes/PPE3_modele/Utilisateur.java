package PPE3_classes.PPE3_modele;

public class Utilisateur {

    public static Utilisateur u = null;

    int numPro;
    String nom;
    String email;
    Character typePro;
    String identifiant;
    String motDePasse;


    public Utilisateur(int numPro, String typePro, String nom, String email, String identifiant, String motDePasse) {
        this.numPro = numPro;
        this.nom = nom;
        this.email = email;
        this.typePro = typePro.charAt(0);
        this.identifiant = identifiant;
        this.motDePasse = motDePasse;
    }

    public int getNumPro() {
        return numPro;
    }
    public String getNom() {
        return nom;
    }
    public  String getEmail() {
        return email;
    }
    public Character getTypePro() {
        return  typePro;
    }
    public String getIdentifiant() {
        return identifiant;
    }


    @Override
    public String toString(){
        return "Main : " + nom + " " + email;
    }

    public boolean estTechnicien() {
        if (typePro == 'T') {
            Techniciens.lesTechniciens.add(this);
            return true;
        } else {
            return false;
        }
    }
}

