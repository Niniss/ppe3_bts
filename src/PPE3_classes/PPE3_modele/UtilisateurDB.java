package PPE3_classes.PPE3_modele;

import javax.rmi.CORBA.Util;
import java.sql.*;

public class UtilisateurDB {

    static String host = "127.0.0.1";
    static String dbName = "ppe";
    static String login = "java1";
    static String motDePasse = "MotDePasse1";
    static int port = 3306;
    static String URL = "jdbc:mysql://" + host + ":" + port + "/" + dbName + "?serverTimezone=Europe/Paris";

    /*
    String URL = "jdbc:mysql://" + host + ":" + port + "/" + dbName + "?serverTimezone=Europe/Paris";
     String host = "127.0.0.1";
        String dbName = "ppe";
        String username = "java1";
        String password = "MotDePasse1";
        String strSQL = "SELECT nom FROM professionnels;";
        int port = 3306;
     */

    public static Utilisateur infoUtilisateur(String login, String motDePasse) {
        //une méthode get qui renvoie un utilisateur
        String strSQL = "SELECT numPro, nom, mail, typePro, identifiant FROM professionnels WHERE identifiant = ? AND mdp = ?;";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection = DriverManager.getConnection(URL, UtilisateurDB.login, UtilisateurDB.motDePasse);

            PreparedStatement pstmt = connection.prepareStatement(strSQL);
            pstmt.setString(1, login);
            pstmt.setString(2, motDePasse);

            ResultSet rset = pstmt.executeQuery();

            if (rset.next() ){
                // récupération des informations de l'utilisateur
                String nom = rset.getString("nom");
               // compléter les variables numPro, nom, mail, statut
                int numPro = rset.getInt("numPro");
                String mail = rset.getString("mail");
                String typePro = rset.getString("typePro");
                String identifiant = rset.getString("identifiant");



                Utilisateur.u = new Utilisateur(numPro, typePro, nom, mail, identifiant, motDePasse);

                if(rset.next() ) {
                    // erreur : deux utilisateurs ont le meme couple login/mdp
                    Utilisateur.u = null;
                }
            }
        }
        catch (ClassNotFoundException e)  {
            e.printStackTrace();
        }
        catch (SQLException e)  {
            e.printStackTrace();
        }
        return Utilisateur.u;
    }
}
