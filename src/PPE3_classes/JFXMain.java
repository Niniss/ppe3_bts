package PPE3_classes;

import java.awt.*;
import java.io.IOException; //Exceptions java???

import PPE3_classes.PPE3_modele.Utilisateur;
import PPE3_classes.PPE3_vues.ControleurActions;
import PPE3_classes.PPE3_vues.ControleurConnexion;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class JFXMain extends Application {

    private Stage primaryStage;
    //private
    public static BorderPane rootLayout;

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Interface Main");

        ouvrirFenetreDeBase(); //Ouvre la fenêtre de base
        //ouvBaseNvTcket();       //Ouvre une autre fenêtre de base
        afficherConnexion(); //Affiche la fenêtre de cnx


        Button bClico = ControleurConnexion.getbClico();
        //bClico.addActionListener(this); // lorsque l'utilisateur confirme son couple ide/mdp...

        /*Boolean clico = ControleurConnexion.getClico();

        if (clico == true) {
            Button nvTicket = ControleurConnexion.getNvTcket();
        }*/
        }


    public void ouvrirFenetreDeBase() {
        try {
            //Ouvre la FenetreDeBase
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(JFXMain.class.getResource("PPE3_vues/FenetreDeBase.fxml"));
            rootLayout = (BorderPane) loader.load();

            // montre la scene contenant la FenetreDeBase
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*public void ouvBaseNvTcket() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(JFXMain.class.getResource("PPE3_vues/FenetreDeBase.fxml"));
            ControleurActions.laBase = (BorderPane) loader.load();

            // montre la scene contenant la FenetreDeBase
            Scene nvScene = new Scene(ControleurActions.laBase);
            primaryStage.setScene(nvScene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/


    public void afficherConnexion() {
        try {
            //ouvre la fenetre de connexion
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(JFXMain.class.getResource("PPE3_vues/Connexion.fxml"));
            AnchorPane fenetreConnexion = (AnchorPane) loader.load();

            // Place la fenetre de connexion au centre de la fenetre de base
            rootLayout.setCenter(fenetreConnexion);
            ControleurConnexion ctl = loader.getController();
            ctl.setMainApp(this);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }



}
