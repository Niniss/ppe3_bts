package PPE3_classes.PPE3_vues;

import PPE3_classes.JFXMain;
import PPE3_classes.PPE3_modele.Utilisateur;
import PPE3_classes.PPE3_modele.UtilisateurDB;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

import java.awt.*;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Scanner;

import java.awt.event.ActionEvent;


public class ControleurConnexion {


    private BorderPane laBase;
    @FXML
    TextField ide;

    @FXML
    TextField mdp;

    @FXML
    static Button bClico;
    static Button nvTcket;

    static Boolean clico = new Boolean(null);

    JFXMain mainApp;


    public static Button getbClico() {
        return bClico;
    }
    public static Button getNvTcket() {
        return nvTcket;
    }

    public static Boolean getClico() {
        return clico;
    }

    @FXML
    private boolean clicConfirme() {

        clico = null;
        String login = ide.getText();
        String motDePasse = mdp.getText();
        UtilisateurDB.infoUtilisateur(login, motDePasse);

        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.initOwner(mainApp.getPrimaryStage());
        alert.setTitle("UTILISATEUR");
        if (Utilisateur.u == null) {
            alert.setHeaderText("Utilisateur introuvable");
            alert.setContentText("Entrez un utilisateur valide.");
            clico = false;
        } else {
            alert.setHeaderText("Utilisateur connecté!");
            alert.setContentText(Utilisateur.u.toString());

            clico = true;
            ouvrirBienvenue();
        }

        alert.showAndWait();
        return clico;
    }

    @FXML
    private void ouvrirBienvenue() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(JFXMain.class.getResource("PPE3_vues/Bienvenue.fxml"));
            AnchorPane Bienvenue = loader.load();

            JFXMain.rootLayout.setCenter(Bienvenue);
            this.setMainApp(mainApp);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void setMainApp(JFXMain mainApp) {
        this.mainApp = mainApp;
    }

}
