package PPE3_classes.PPE3_vues;

import PPE3_classes.JFXMain;
import PPE3_classes.PPE3_modele.Ticket;
import PPE3_classes.PPE3_modele.Utilisateur;
import PPE3_classes.PPE3_modele.UtilisateurDB;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

import javax.rmi.CORBA.Util;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;

public class ControleurActions implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {

    }

    JFXMain mainApp;

    public static BorderPane laBase;

    public void setMainApp(JFXMain mainApp) {
        this.mainApp = mainApp;
    }

    @FXML
    TextField numProduit;

    @FXML
    TextField description;


    @FXML
    private void clicMesTickets() {
        Ticket.afficherTickets();

    }

    @FXML
    private void clicNvTcket() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(JFXMain.class.getResource("PPE3_vues/OuvertureTicket.fxml"));
            AnchorPane ouvertureTicket = loader.load();

            JFXMain.rootLayout.setCenter(ouvertureTicket);
            this.setMainApp(mainApp);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void appelerOtto() {
        //try {

            int numTicket = -1;
            int numProduit = Integer.parseInt(this.numProduit.getText());
            String statut = "O";

            int numPro = Utilisateur.u.getNumPro();

            Date dateTicket = new Date();
            Date clotTicket = null;
            String description = this.description.getText();
            clicNvTcket();
            int complex = -1;
            int numTechnicien = -1;
            Ticket t = new Ticket(numTicket, numProduit, statut, numPro, dateTicket,
                    clotTicket, description, complex, numTechnicien);

            boolean otto = Ticket.ouvrirTicket(t);

            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("UTILISATEUR");
            if (otto) {
                alert.setHeaderText("Données insérées");
                alert.setContentText("Ok");
            } else {
                alert.setHeaderText("Données non insérées!");
                alert.setContentText("erreur");
            }

            alert.showAndWait();

             //test : réaffiche la page nv ticket
        //}
        /*catch (SQLException e) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setHeaderText("Utilisateur connecté!");
            alert.setContentText(e.getSQLState());
        }*/
    }

}
